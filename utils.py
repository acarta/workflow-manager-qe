import numpy as np 
import pandas as pd

def get_path_range(k_latt, k_dict, k_dens = 20):

    """
    Reads a pymatgen k-path, computes its length
    and returns a range of k values.

    Args:
        k_latt (numpy array): matrix of the reciprocal lattice
            example: 
            array([
            [1.        , 0.        , 1.41421356],
            [0.        , 1.        , 1.41421356],
            [0.        , 0.        , 1.        ]])

        kpath_dict (dict): Dictionary containing the high symmetry points
            in the kpath for plotting of the band structures, requires a pymatgen KPathSetyawanCurtarolo
            dictionary, below an example:
            
            kpath_dict
            {'kpoints': {
            '\\Gamma': array([0., 0., 0.]),
            'X': array([0. , 0.5, 0. ]),
            'R': array([0.5, 0.5, 0.5]),
            'M': array([0.5, 0.5, 0. ])},
            'path': [['\\Gamma', 'X', 'M', '\\Gamma', 'R', 'X'], ['M', 'R']]} 
        kpath_dens (int): number of points along a piece of the kpath, to use
            in conjunction with kpath_dict

    Returns:
        numpy array
    """
    n_points = (len(k_dict['path'][0]))
    tot_len = 0
    # calculate the metric tensor for reciprocal space so that you can 
    # compute the length of vectors in crystal coordinates
    metr_tensor = np.matmul(np.transpose(k_latt), k_latt)
    
    k_range = np.array([]) #initialize the krange vector

    for i in range(n_points-1):
        if i == (n_points-2): #this includes the last point
            subdivisions = k_dens + 1
            include_last = True #include the last point in the last part of the path
        else:
            subdivisions = k_dens
            include_last = False
        key = k_dict['path'][0][i]
        key_next =  k_dict['path'][0][i+1]

        startpoint = k_dict['kpoints'][key]
        endpoint = k_dict['kpoints'][key_next]

        k_vec = endpoint - startpoint
        k_len = np.sqrt(np.dot(k_vec,np.matmul(metr_tensor, k_vec)))


        loc_range = np.linspace(tot_len, tot_len + k_len, subdivisions, endpoint=include_last)
        tot_len = tot_len + k_len 
        k_range = np.append(k_range, loc_range)

    return k_range

def gnuplot_conv(filepath, col=0, separator = "  "):
        
    #load the relevant colums of the data
    df = pd.read_csv(filepath, skip_blank_lines=False,
                        header=None, usecols=[col], sep = separator)
    df2 = df
    #The different bands are evenly spaced with empty rows,
    #count the number of empty rows to get the number of bands
    emptydf = df.isnull()
    n_gnu_bands = np.sum(np.array(emptydf))
    #find the number of k-point, the -1 accounts for the empty lines
    print(f'number of intervals detected: {n_gnu_bands}')
    gnu_kpts = int( len(emptydf) / n_gnu_bands) - 1

    #drop the Nan in the original dataframe
    df = df.dropna()


    gnu_band_data = np.zeros((gnu_kpts, n_gnu_bands))
    for band in range(n_gnu_bands):
        this_band = np.array(df.iloc[(band*gnu_kpts):((band+1)*(gnu_kpts))])
        gnu_band_data[:, band] = this_band.transpose()

    return gnu_band_data  
    