import numpy as np
import os
# import json

from pymatgen import Lattice, Structure, Molecule
# import pymatgen.vis.structure_vtk as vtk 
import pymatgen.io.pwscf as qeIO
from pymatgen.io.pwscf import PWInput
import pymatgen.analysis.elasticity.strain as pymgstr
import pymatgen.symmetry.kpath  as kpath
import qe_parser as parse

class PWgenerator:
    """
    Creates class for the serial generation of input files for quantum espresso.
    which interfaces with the PWInput class of pymatgen

    Args:
        Struct (Pymatgen Structure): Pymatgen Input structure.
        input_dict (dict): A dictionary with the Quantum espresso parameters
        filename (string): Prefix name of the files
        filepath (string): Path to the directory where you want the files saved
        MUST end with /
    Additional modifiers:    

        k_range (list): range of values for the x direction in the k grid
        cutoff (int): energy cutoff in Ry
        kfunc (fun): function that returns a list of 3 integers given the first element
        corresponding to the x direction
    """
    def __init__(self,Struct,
                input_dict,
                filename=None,
                filepath=None,
                kgrid = [8,8,8]):
        self.Struct = Struct
        self.input_dict = input_dict
        self.filename = filename
        self.filepath = filepath
        
        
        #default values for energy cutoff parameters
        self.cutoff_default = 48
        self.cutoff_range = range(40, 84, 4)
        #Default values for k-grid parameters 
        self.k_grid = kgrid 
        self.k_range = range(4,16,2) #range of value for x direction of the Monkhorst-Pack grid
        self.k_func = lambda x : [x,x,x]
        #default strain parameters
        self.eps_range = np.arange(-1,1.5,0.5) 
        self.eps_func = eps_func_std #function that takes a value and returns a strain matrix
        #default Hubbard U parameters
        self.u_list =  [False]*20
        self.u_range = range(1,7)
        self.u_places = [4,5,6,7] #index of the atoms where the Hubbard U correction is considered
        #default constrained moment calculation
        self.mom_range = np.linspace(0, 2, 9)
    def generate_conv_cutoff(self,subfolder = ''):
        """
        Creates a series of input files for energy cutoff convergence tests.
        
        #####################
        Using optional variables:
            * filename
            * filepath
            * cut_range 
            * kgrid
        """
        filepath = self.filepath + subfolder
        local_dict = self.input_dict

        print('making energy convergence folders')
        for en_cut in self.cutoff_range:
            en_rho = en_cut*12
            os.makedirs(filepath, exist_ok=True)
            local_dict['system']['ecutwfc'] = en_cut
            local_dict['system']['ecutrho'] = en_rho


            to_qe = qeIO.PWInput(self.Struct,
                                control=local_dict['control'],
                                system= local_dict['system'],
                                electrons = local_dict['electrons'],
                                kpoints_grid=self.k_grid
                                )
            to_qe.write_file(filepath + f'{self.filename}_en_cut_{en_cut}.in')

    
    def generate_conv_kgrid(self, subfolder = ''):
        """
        Creates input files for k convergence 
        #####################
        Using optional variables:
            * filename
            * filepath
            * k_range  
            * k_func
        """
        filepath = self.filepath + subfolder
        local_dict = self.input_dict

        if not isinstance(self.k_range, (list, range)):
            raise ValueError("Unsupported type for k_range, must be list or range")
        if not callable(self.k_func):
            raise ValueError("Unsupported type for k_func, must be function")
        
        print('making k convergence folders')
        for ksp in self.k_range:
            k_grid = self.k_func(ksp)
            os.makedirs(filepath, exist_ok=True)



            to_qe = qeIO.PWInput(self.Struct,
                                control=local_dict['control'],
                                system= local_dict['system'],
                                electrons = local_dict['electrons'],
                                kpoints_grid=k_grid
                                )
            to_qe.write_file(filepath + f'{self.filename}_k_grid={k_grid}.in')
        pass
    def generate_strain_scan(self, subfolder = ''):
        """
        Creates input files for strain dependent 
        #####################
        Using optional variables:
            * filename
            * filepath
            * eps_range: range of stresses one wants to apply 
            * eps_func: function that gets one input and generates a stress matrix
        """
        filepath = self.filepath + subfolder
        local_dict = self.input_dict
        
        os.makedirs(filepath, exist_ok=True)
        for eps in self.eps_range:
            eps_M_new = self.eps_func(eps)

            deform = pymgstr.convert_strain_to_deformation(
                pymgstr.Strain(eps_M_new)
            )

            Struct_str = deform.apply_to_structure(self.Struct)

            to_qe = qeIO.PWInput(Struct_str,
                            control=local_dict['control'],
                            system= local_dict['system'],
                            electrons = local_dict['electrons'],
                            cell= local_dict['cell'],
                            kpoints_grid=self.k_grid
                            )    
            to_qe.write_file(f'{filepath}/{self.filename}_strain_{np.round(eps,2)}percent.in')
    def generate_U_scan(self, subfolder=''):
        """
        Creates input files for hubbard U relaxation
        #####################
        Using optional variables:
            * filename
            * filepath
            * u_range  
        """
        filepath = self.filepath + subfolder
        local_dict = self.input_dict
        local_u_list = self.u_list        

        for u_val in self.u_range:
            for place in self.u_places: 
                local_u_list[place] = float(u_val)
                # print(local_u_list) 
            self.Struct.add_site_property('Hubbard_U', local_u_list) 
            os.makedirs(filepath, exist_ok=True)
        
            local_dict['control']['calculation'] = 'vc-relax'
            to_qe = qeIO.PWInput(self.Struct,
                                control=local_dict['control'],
                                system= local_dict['system'],
                                electrons = local_dict['electrons'],
                                kpoints_grid=self.k_grid
                                )
        
            to_qe.write_file(f'{filepath}{self.filename}_U{u_val}.in')
    
    def generate_mom_scan(self, start_list, constrain_type='total', subfolder=''):
        """
        Creates input files for constrained moment calculation
        #####################
            * start_list: 0s for the atoms without momenta and 1/-1 for the atoms with momenta 
            ex [0, 1, 0, 0, -1] +1*mom on the 2nd atom and -1*mom on the last
        #####################
        Using optional variables:
            * filename
            * filepath
            * mom_range  
        """
        filepath = self.filepath + subfolder
        local_dict = self.input_dict
        # local_dict['system']['constrained_magnetization'] = constrain_type
        for mom in self.mom_range:
            local_mom_list = list(map(lambda x: x*mom, start_list))
            self.Struct.add_site_property('starting_magnetization', local_mom_list)
            
            # self.Struct.add_site_property('nspin', 4)
            os.makedirs(filepath, exist_ok=True)
        
            local_dict['control']['calculation'] = 'scf'
            local_dict['system']['tot_magnetization'] = mom
            to_qe = qeIO.PWInput(self.Struct,
                                control=local_dict['control'],
                                system= local_dict['system'],
                                electrons = local_dict['electrons'],
                                kpoints_grid=self.k_grid
                                )
        
            to_qe.write_file(f'{filepath}{self.filename}_M{np.round(mom,2)}.in')
#   Gener
        
def k_func_std(ksp):
    """
    returns a normal cubic grid
    Args: 
        ksp(int): number of grid points
    """
    return [ksp, ksp, ksp]

def k_func_zratio(ksp, zratio = 2):
    """
    returns a k-grid adapted for a cell with a certain aspect ratio

    Args: 
        ksp (int): number of grid points in the x direction
        zratio (float): z/x ratio
    """
    return [ksp, ksp, int(1/zratio*ksp)]

def eps_func_std(eps):
    """
    Hydrostatic 1% strain
    """
    eps_M = np.array( [
                        [0.01,  0, 0],
                        [0,  0.01, 0],
                        [0,  0, 0.01],
                        ])
    return eps*eps_M

def eps_func_planar(eps):
    """
    Planar xy 1% strain
    """
    eps_M = np.array( [
                        [0.01,  0, 0],
                        [0,  0.01, 0],
                        [0,  0, 0],
                        ])
    return eps*eps_M

def update_from_relax(inp_struct, parsed_output, update_lattice = True, update_atoms=True):
    assert type(inp_struct) == Structure, "Not a pymatgen structure"
    assert type(parsed_output) == parse.ParseQEO, "Not a ParseQEO class"
        
    def remove_numbers(my_string):
        newstring = ''.join([i for i in my_string if not i.isdigit()])
        return newstring
    
    new_struct = inp_struct

    if update_lattice:
        newlatt = Lattice([ parsed_output.lattice['a'],
                            parsed_output.lattice['b'],
                            parsed_output.lattice['c']])

        # new_struct.modify_lattice(new_lattice = newlatt)
        new_struct.lattice = newlatt

    if update_atoms:
        natoms = parsed_output.natoms
        
        new_struct.remove_sites(range(0, natoms))
        
        site_names = [parsed_output.atoms[i][0] for i in range(0,natoms)]
        site_names = list(map(remove_numbers, site_names))
        site_coord = [parsed_output.atoms[i][1:] for i in range(0,natoms)]

        for i in range(0,natoms):
            new_struct.append(site_names[i], site_coord[i])
    
    return new_struct

