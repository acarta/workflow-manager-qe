
import sys
import numpy as np
import json
import linecache
from scipy.stats import norm as gaussian
import pandas as pd 
import re
import time

#provaprova

def convolute_dos(KS_eigs, weight_array, minim, maxim, length = 3000, gamma = 0.05):

    """Calculates the state_frame object from the parsed file. 
    
    Parameters
    ----------
    KS_eig : vector with all the Kohn-Sham eigenvalues
    
    weight_array : vector with the weighting of the KS eigenvalues 
                   due to symmetry or orbital participation

    minim: minimum of the energy (in eV)

    maxim: maximum of the energy (in eV)    

    length = dimension of dos array

    gamma = (default = 0.01) standard deviation of the gaussian
            used for the convolution

    Returns
    ------
    
    dos : 1D array containing the DOS

    en_space : 1D array: energy grid 

    """
    en_space = np.linspace(minim , maxim, length)
    dos_smoothed = np.zeros(length)

    range_iter = np.where(np.logical_and(KS_eigs >=minim, KS_eigs<=maxim))[0]
    # for i in range(len(dos_deltas)):
    # print(range_iter)
    for i in range_iter:
      # print(KS_eigs[i])
      # print(weight_array[i])
      dos_smoothed = dos_smoothed + gaussian.pdf(en_space, KS_eigs[i], gamma )* weight_array[i]
    
    return dos_smoothed, en_space

class ParsePROJWFC:
  """
    General considerations
    ----------
    Parser for the projwfc code, for this to work you must set
    in your projwfc input file:
      >> filproj = 'SAMPLE_NAME', (see **)
    
    This class will take as file/array inputs:
      >> SAMPLE_NAME.out from the projwfc calculation
      >> SAMPLE_NAME.projwfc_up from the projwfc optional** output file
      >> SAMPLE.bnddiagram as band data from the ParseQEO() class
    Other inputs (all available from ParseQEO() ):
      >> bands : number of bands
      >> kpts : number of k-points
      >> natoms : number of atoms in the unit cell
    
    Example of using this class:
    ----------
      
      parsed_data = parse.ParseQEO()
      parsed_data.Process("SAMPLE_NAME_PWX.out")
      
      bands = parsed_data.bands
      kpts = parsed_data.kpts
      natoms = parsed_data.natoms
      band_data = parsed_data.bnddiagram

      class_sample = parse.ParsePROJWFC(bands,kpts,natoms)
      class_sample.make_state('SAMPLE_NAME_PROJWFC.out')
      class_sample.make_proj('SAMPLE_NAME_PROJWFC.projwfc_up', band_data)
      class_sample.make_orb_simple()
    
  """  

  def __init__(self, bands, kpts, natoms):
    
    columns_state = ['State #', 'Atom #', 'Element', 'wfc', 'l', 'm', 'm_index' ]
    self.columns_proj = ['k_index','n_band','KS Eigenenergy', '|Psi|^2']
    
    self.state_frame = pd.DataFrame(columns=columns_state)
    self.proj_frame = pd.DataFrame(data=None) 
    self.orb_frame = pd.DataFrame(data=None) 
    self.dos_frame = pd.DataFrame(data=None) 
    self.filename_out = ''
    self.filename_proj = ''
    self.name_list = [] #column nameswith element and orbital from the orb fram
    self.kpts = kpts
    self.bands = bands
    self.natoms = natoms
  def make_state(self, filename_out, SOC=False):
    """Calculates the state_frame object from the parsed file. 
    
    Parameters
    ----------
    filename_out : path to the .out output file of a projwfc.x calculation

    Returns
    ------
    state_frame : updated pandas dataframe displaying the enumerated state
    the element, angular and azimutal quantum numbers
    """
    linenum = 0
    maxlines = sum(1 for line in open(filename_out)) #get maximum number of lines in the file
    k_idx = 1

    while linenum < maxlines: #use linenum as an index to cycle throughout the entire file
        line  = linecache.getline(filename_out ,linenum)
        #print(str(linenum)+'  ' + line)
        if ('Atomic states used for projection') in line: #once you get to the state indexing loop all over them
            print('Banana')
            linenum = linenum+3 #skip three lines from the quantum espresso output
            #stateline = linecache.getline('SCO_pdos.out' ,linenum)
        elif 'state #' in line:
            print(line)
            #sample stateline:
            #  state #  14: atom   2 (Sr ), wfc  2 (l=1 m= 3)
            ### For SOC
            #  state #   1: atom   1 (Pb ), wfc  1 (l=1 j=0.5 m_j=-0.5)
            #print(stateline)
            This_entry = {} #create an empty dictionary to put the regex matched data in
            This_entry['State #'] = int(re.findall("(?<=#)(.*)(?=:)", line)[0]) #matches all strings between '#' and ':': '  14' in example
            This_entry['Atom #'] = int(re.findall("(?<=atom)(.*?)(?=\()", line)[0]) #matches all strings between 'atom' and '(', the (.*?) is non greedy, so it matches only the first occurence: '  2' in example
            This_entry['Element'] = (re.findall("(?<=\()(.*?)(?=\))", line)[0]).replace(" ", "") #matches all strings between '(' and ')', the (.*?) is non greedy, so it matches only the first occurence: 'Cr  ' in example
            This_entry['wfc'] = int(re.findall("(?<=wfc)(.*?)(?=\()", line)[0]) #matches all strings between '(' and ')', the (.*?) is non greedy, so it matches only the first occurence: 'Cr  ' in example
            This_entry['l'] = int(re.findall("\(l=(\d+)", line)[0]) #matches all numbers strings after the group '(l=',  '1' in example
            if SOC == False:
              This_entry['m_index'] = int(re.findall("m= (\d+)", line)[0]) #matches all numbers strings after the group '(m= ',  '3' in example
              This_entry['m'] = This_entry['m_index']-(This_entry['l']+1) #matches all numbers strings after the group '(m= ',  '3' in example
            else:
              This_entry['j'] = (re.findall("j=([ -]?([0-9]*[.])?[0-9]+) ", line)[0][0]) #matches all numbers strings after the group '(j= ',  '0.5' in example
              This_entry['m_index'] = (re.findall("m_j=([ -]?([0-9]*[.])?[0-9]+)", line)[0][0]) #matches all numbers strings after the group '(m_j= ',  '-0.5' in example



            self.state_frame = self.state_frame.append(This_entry, ignore_index=True)

            linenum += 1
            stateline = linecache.getline('SCO_pdos.out' ,linenum)
        

        elif ('k =') in line:

            print('Built state_frame')
            break

        else:
            linenum += 1
  
  def make_proj(self, projwfc_out, band_data, Debug=False):

    """Calculates the state_frame object from the parsed file. 
    
    Parameters
    ----------
    projwfc_out : path to the .projwfc_up output file of a projwfc.x calculation
    
    band_data : array of size (kpts, bands) containing the KS
    eigenvalues
    
    Returns
    ------
    proj_frame : updated pandas dataframe displaying the projection coefficient
    of every state onto localized atomic states (distinguishing l, m)

    """
    
    self.proj_frame = pd.DataFrame(0.0,
        index=range(self.bands*self.kpts),
        columns= (self.columns_proj+list(self.state_frame['State #'] ))
        )
    
    linenum = 0
    while True:
        linenum += 1
        if '    F    F' in linecache.getline(projwfc_out ,linenum): #for normal collinear calculations
            print('start parsing') 
            start = time.time()
            break
        elif '    T    T' in linecache.getline(projwfc_out ,linenum): #for non collinear stuff
            print('start parsing') 
            start = time.time()
            break


    # if Debug:
    #     print(linecache.getline(projwfc_out ,linenum))
    multipl = self.bands*self.kpts
    s_idx=1
    orb_list = []
    for state in self.state_frame['State #']:
        linenum +=1
        #append the orbital name to the namelist
        orb_list.append(
            linecache.getline(projwfc_out ,linenum).split()[3]
            )
        
        print(f'Processing state #{s_idx}')
        self.proj_frame[state]=np.loadtxt(
          projwfc_out, skiprows=linenum,
          max_rows=multipl, usecols=(2))
        #the numpy parser is much faster
        # pd.read_csv(
        #     projwfc_out, skiprows=linenum,
        #     nrows=multipl, header=None,delimiter='  '
        #     ).iloc[:,-1:]
        linenum += multipl
        s_idx += 1
    finish = time.time()
    print(f'The parsing was finished in {finish-start} s')
    #calcualate the norm^2 of the wavevector by summing
    #over the states
    self.proj_frame['|Psi|^2'] = self.proj_frame[list(self.state_frame['State #'])].sum(axis=1)
    #fill in the blanks of the rest of the dataframe
    self.proj_frame['k_index'] = np.repeat(range(1, self.kpts+1), self.bands)
    self.proj_frame['n_band'] = np.tile(range(1, self.bands+1), self.kpts)
    self.proj_frame['KS Eigenenergy'] = band_data.flatten()

    #add the orbital legend to the state frame
    self.state_frame['Orbital'] = orb_list
  
  def make_orb_simple(self):

    """Calculates the state_frame object from the parsed file. 
    
    Returns
    ------
    orb_frame : updated pandas dataframe displaying the projection coefficient
    of every state onto the localized atomic orbitals

    """
    ident_list = [] #groups the indexes of the equivalent wavefunctions 
    state_list = [] #state number of the list to retrieve in 

    for atom in range(1,self.natoms+1):
        atom_frame = self.state_frame.loc[self.state_frame['Atom #'] == atom ]
        wfc_list = atom_frame.wfc.unique()

        
        
        for i in wfc_list:
            local_list =list( 
                atom_frame.loc[atom_frame['wfc'] == i].index
                )
            
            ident_list.append(local_list)

    for i in ident_list:
        
        atom_name = self.state_frame.loc[i[0]]['Element'] #the first element of the list is sufficient
        atom_number= self.state_frame.loc[i[0]]['Atom #']
        atom_orb = self.state_frame.loc[i[0]]['Orbital']
        atom_l = self.state_frame.loc[i[0]]['l']


        string_label = f'{atom_name}_#{atom_number}_{atom_orb}'
        self.name_list.append(string_label)
        state_list.append(list(self.state_frame.loc[i]['State #']))

    #create now an orbital resolved (Orb) dataframe and copy the 

    self.orb_frame = pd.DataFrame(0.0, index=range(self.bands*self.kpts),columns= (self.columns_proj + self.name_list))
    self.orb_frame[self.columns_proj] = self.proj_frame[self.columns_proj]

    #cop
    for i in range(len(self.name_list)):
        self.orb_frame[self.name_list[i]] = (
            self.proj_frame[state_list[i]].sum(axis=1)
        )
  def  make_dos(self, sym_weight, minim, maxim, length = 3000, gamma = 0.01):
    """
    wrapper for the convolute_dos function

    """
    if self.orb_frame.empty:
      raise Exception("Sorry, load/make your orbital frame first")

    else:
      Eigs = np.array(self.orb_frame['KS Eigenenergy'])
      start = time.time()
      dos_namelist = list(self.orb_frame.drop(columns=self.columns_proj))[0:]
      
      self.dos_frame = pd.DataFrame(0.0, index = range(length), columns=(['Energy', 'DOS_total'] + dos_namelist))
      
      [dos_total, en_space] = convolute_dos(Eigs, sym_weight, minim, maxim, length, gamma) 
      self.dos_frame['DOS_total'] = dos_total
      self.dos_frame['Energy'] = en_space      

      for column in dos_namelist:
        print(f'Convoluting state {column}')
        weight = np.array(sym_weight*self.orb_frame[column])
        [dos, _] = convolute_dos(Eigs, weight, minim, maxim, length, gamma)
        self.dos_frame[column] = dos 
        
        # print(column)
      end = time.time()
      print(f'Convoluting the dos took {end-start} s')



    return None







#God Bless this script
#Created by Levi Lentz
#Covered under the MIT license
class ParseQEO:
  def __init__(self):
    self.BOHRtoA = 0.529177249
    self.RYtoeV = 13.605698066
    self.program_version = ""
    self.volume = 0.0
    self.alat = 0.0
    self.natoms = 0
    self.nat = 0
    self.nelect = 0
    self.Ecut = 0.0
    self.RhoCut = 0.0
    self.Econv = 0.0
    self.beta = 0.0
    self.Exch = ""
    self.energy = 0.0
    self.natoms = 0
    self.bandgap = 0.0
    self.bands = 0
    self.lattice = {'a':np.zeros(3),'b':np.zeros(3),'c':np.zeros(3)}
    self.atoms = []
    self.norms = {'a':0.0,'b':0.0,'c':0.0}
    self.angles = {'alpha':0.0,'beta':0.0,'gamma':0.0}
    self.kpts = 0
    self.bnddiagram = np.zeros(0)
    self.symweights = np.zeros(0)
    self.FermiTest = False
    self.Fermi = 0.0
    self.U_dict = {} #dictionary for Hubbard U calculations
  def Process(self,filestring, spin_polarize=False, Hubbard_U=False, Debug= False):

    f = open(filestring,'r')
    linenum = 0
    #'i' stands for the line in this script
    for i in f:
      # debug mode
      if Debug: 
        print(i)

      if linenum < 1000:
        if "number of k points=" in i:
          self.kpts = int(i.split()[4])
          ##new code for DOS
          self.symweights = np.zeros(self.kpts)
          next(f)
          for j in range(self.kpts):
            line= next(f)
            #print(line)
            self.symweights[j] = float(line.split()[9])
            
          

          next
        if "Program PWSCF" in i:
          self.program_version = i.split()[2]
          next
        if "lattice parameter (alat)" in i:
          self.alat = float(i.split()[4])*self.BOHRtoA
          next
        if "number of Kohn-Sham states" in i:
          self.bands = int(i.split()[4]) 
        if "unit-cell volume" in i and "new" not in i:
          self.volume = float(i.split()[3])*(self.BOHRtoA**3.0)
          next
        if "number of atoms/cell" in i:
          self.natoms = int(i.split()[4])
          next
        if "number of atomic types" in i:
          self.nat = int(i.split()[5])
          next
        if "number of electrons" in i:
          self.nelect = float(i.split()[4])
          next
        if "kinetic-energy cutoff" in i:
          self.Ecut = float(i.split()[3])*self.RYtoeV
          next
        if "charge density cutoff" in i:
          self.RhoCut = float(i.split()[4])*self.RYtoeV
          next
        if "convergence threshold" in i:
          self.Econv = float(i.split()[3])
          next
        if "mixing beta" in i:
          self.beta = float(i.split()[3])
          next
        if "Exchange-correlation" in i:
          self.Exch = i.split()[0]#should be [2]
          next
        if "a(1) =" in i:
          tmp = i.split()
          for j in range(0,3):
            self.lattice['a'][j] = tmp[j+3]
          next
        if "a(2) =" in i:
          tmp = i.split()
          for j in range(0,3):
            self.lattice['b'][j] = tmp[j+3]
          next
        if "a(3) =" in i:
          tmp = i.split()
          for j in range(0,3):
            self.lattice['c'][j] = tmp[j+3] 
          next
        if "site n.     atom                  positions (alat units)" in i:
          for j in range(0,self.natoms):
            line = next(f).split()
            self.atoms.append([line[1],float(line[6])*self.alat,float(line[7])*self.alat,float(line[8])*self.alat])
          next
      if "!" in i:
        self.energy= float(i.split()[4])*self.RYtoeV
      if "new unit-cell volume" in i:
        self.volume = float(i.split()[4])*(self.BOHRtoA**3)
      
      if "Begin final coordinates" in i:
        while "End final coordinates" not in line:
          line = next(f)
          if "CELL_PARAMETERS" in line:
            for j in ['a','b','c']:
              line = next(f)
              tmp = line.split()
              for k in range(0,3):
                self.lattice[j][k] = float(tmp[k])
          if "ATOMIC_POSITIONS" in line:
            if "(crystal)" in line:
              for j in range(0,self.natoms):
                line = next(f).split()
                self.atoms[j] = [line[0],float(line[1]),float(line[2]),float(line[3])]
            # if "angstrom" in line:
            #   for j in range(0,self.natoms):
            #     line = next(f).split()
            #     self.atoms[j] = [line[0],float(line[1]),float(line[2]),float(line[3])]
      if Hubbard_U:
        if "Tr[ns(na)] (up, down, total)" in i:
          this_line = i.split()
          natom = int(this_line[1])
          up_num = float(this_line[-3])
          down_num = float(this_line[-2])
          tot_num = float(this_line[-1])
          
          self.U_dict[f'atom {natom}'] = {
            'natom': natom,
            'up_num': up_num,
            'down_num': down_num,
            'tot_num': tot_num
          }

          for spin_idx in ['up', 'down']: 
            # print(f'Processing spin {spin_idx} for atom {natom}')
            next(f), next(f)
            line = next(f)
            eigs = line.split()
            self.U_dict[f'atom {natom}'][f'{spin_idx} occupations'] = eigs

            for skipline in range(len(eigs)*1+2): #temporary, skip all the eigenvectors and the occupation matrix plus the headers
              next(f)
            
            temp_mat = np.zeros((len(eigs), len(eigs)))
            for mat_idx in range(len(eigs)):
              line = next(f)
              # print (line)
              temp_mat[mat_idx] = line.split()
            self.U_dict[f'atom {natom}'][f'{spin_idx} occupation matrix'] = temp_mat

          this_line = next(f).split()
          magmom = float(this_line[-1])
          self.U_dict[f'atom {natom}']['local magnetic moment'] = magmom
          self.U_dict[f'atom {natom}']['d states'] = ['r^2-3*z^2', 'xz', 'yz', 'xy', 'x^2-y^2']


          
      

      band_condition = (
        "End of self-consistent calculation" in i or
        "End of band structure calculation" in i 
      )
      

      if band_condition: #The band diagram is stored in lines of 8 entries
        if np.floor(self.bands/8.)*8. <= self.bands:
          numlines = int(np.floor(self.bands/8.) + 1)
          remainder = int(self.bands - np.floor(self.bands/8.)*8.)
        else: 
          numlines = int(np.floor(self.bands/8.))
          remainder = 0
        
        if spin_polarize == True:
          self.bnddiagram =  np.zeros((self.kpts,self.bands, 2))
          spin_iter = [0,1]
        else:
          spin_iter = [0]
          self.bnddiagram = np.zeros((self.kpts,self.bands,1))
        
        for polarization in spin_iter:
          counter = 0
          while counter < self.kpts:
            if 'SPIN' in line:
              print('Parsing: ' + line) 
            line = next(f)

            if "k =" in line:
              line = next(f)
              counter1 = 0
              for j in range(0,numlines):
                line = next(f)
                for k in range(0,len(line.split())):
                  self.bnddiagram[counter][counter1 + k][polarization] = float(line.split()[k])
                counter1 += 8
              counter += 1
          next
        next  
        


      if "highest occupied, lowest unoccupied level (ev)" in i:
        self.bandgap = float(i.split()[7]) - float(i.split()[6])
        next
      if "the Fermi energy is" in i:
        self.Fermi = float(i.split()[4])
        self.FermiTest = True
        next
      linenum += 1
    f.close()
    for i in ['a','b','c']:
      self.norms[i] = np.linalg.norm(self.lattice[i])
    self.angles['alpha'] = np.arccos(np.dot(self.lattice['b'],self.lattice['c'])/(self.norms['c']*self.norms['b'])) * 180./np.pi
    self.angles['gamma'] = np.arccos(np.dot(self.lattice['a'],self.lattice['b'])/(self.norms['a']*self.norms['b'])) * 180./np.pi
    self.angles['beta'] = np.arccos(np.dot(self.lattice['a'],self.lattice['c'])/(self.norms['a']*self.norms['c'])) * 180./np.pi
    if self.FermiTest == True: #The bandgap is now in the band diagram
      self.bnddiagram = np.subtract(self.bnddiagram,self.Fermi)
      emin = np.zeros(self.kpts)
      emax = np.zeros(self.kpts)
      counter = 0
      #NEED TO FIX, BANDGAP IS OBTAINABLE ONLY
      #FOR SINGLE SPIN POLARIZATION
      for j in self.bnddiagram[:,:,0]:
        emin[counter] = j[np.searchsorted(j,  0.0,side='right')-1]
        emax[counter] = j[np.searchsorted(j,  0.0,side='right')]
        counter += 1
      self.bandgap = float(np.min(emax-emin))
  def to_JSON(self, outfile):
    for i in self.lattice:
      self.lattice[i] = self.lattice[i].tolist() 
    self.bnddiagram = self.bnddiagram.tolist() #JSON doesnt like numpy arrays
    return json.dump(self, outfile, default=lambda o: o.__dict__, sort_keys=True, indent=4)
 
# test = ParseQEO()
# test.Process("SCO_nscf.out")
 

# band_data = test.bnddiagram.tolist() 

# with open('jason.txt', 'w') as outfile:
#     ParseQEO.to_JSON(test, outfile)




